FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

RUN apt-get remove -y php-* php8.1-* libapache2-mod-php8.1 && \
    apt-get autoremove -y && \
    add-apt-repository --yes ppa:ondrej/php && \
    apt update && \
    apt install -y php8.2 php8.2-{apcu,bcmath,bz2,cgi,cli,common,curl,dba,dev,enchant,fpm,gd,gmp,gnupg,imagick,imap,interbase,intl,ldap,mailparse,mbstring,mysql,odbc,opcache,pgsql,phpdbg,pspell,readline,redis,snmp,soap,sqlite3,sybase,tidy,uuid,xml,xmlrpc,xsl,zip,zmq} libapache2-mod-php8.2 && \
    apt install -y php-{date,pear,twig,validate} && \
    rm -rf /var/cache/apt /var/lib/apt/lists

RUN apt-get -y update && \
    apt install -y lame ffmpeg mp3splt ubuntu-restricted-extras && \
    rm -rf /var/cache/apt /var/lib/apt/lists

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
ADD apache/ampache.conf /etc/apache2/sites-enabled/ampache.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf

RUN a2enmod expires rewrite

# configure mod_php
RUN crudini --set /etc/php/8.2/apache2/php.ini PHP upload_max_filesize 256M && \
    crudini --set /etc/php/8.2/apache2/php.ini PHP upload_max_size 256M && \
    crudini --set /etc/php/8.2/apache2/php.ini PHP post_max_size 256M && \
    crudini --set /etc/php/8.2/apache2/php.ini PHP memory_limit 256M && \
    crudini --set /etc/php/8.2/apache2/php.ini PHP max_execution_time 200 && \
    crudini --set /etc/php/8.2/apache2/php.ini Session session.save_path /run/ampache/sessions && \
    crudini --set /etc/php/8.2/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/8.2/apache2/php.ini Session session.gc_divisor 100

RUN ln -s /app/data/php.ini /etc/php/8.2/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/8.2/cli/conf.d/99-cloudron.ini

# renovate: datasource=github-releases depName=ampache/ampache versioning=semver
ARG AMPACHE_VERSION=7.3.0

# ampache
RUN wget -O ampache.zip https://github.com/ampache/ampache/releases/download/${AMPACHE_VERSION}/ampache-${AMPACHE_VERSION}_all_php8.2.zip && \
    unzip ampache.zip && \
    rm ampache.zip && \
    chown -R www-data.www-data /app/code

RUN ln -sf /app/data/rest/htaccess /app/code/public/rest/.htaccess && \
    ln -sf /app/data/play/htaccess /app/code/public/play/.htaccess && \
    mv /app/code/public/themes /app/code/public/themes.orig && ln -sf /app/data/themes /app/code/public/themes && \
    mv /app/code/config /app/code/config.orig && ln -sf /app/data/config /app/code/config

COPY ampache.cfg.php.template start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
