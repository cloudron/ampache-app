[0.1.0]
* Initial version

[0.2.0]
* Add packages to help transcoding

[0.3.0]
* Update ampache to 4.2.4
* [Full changelog](https://github.com/ampache/ampache/releases/tag/4.2.4)

[0.4.0]
* Make themes customizable

[1.0.0]
* Update ampache to 4.2.5
* [Full changelog](https://github.com/ampache/ampache/releases/tag/4.2.5)

[1.0.1]
* Update Ampache to 4.2.6
* [Full changelog](https://github.com/ampache/ampache/releases/tag/4.2.6)
* Don't check the times in save_mediaplay plugins
* Plugins should only have 1 category
* Update Composer requirements

[1.1.0]
* Update Ampache to 4.3.0
* [Full changelog](https://github.com/ampache/ampache/releases/tag/4.3.0)
* Check limits on democratic playlists (> 0 && < 3000000000)
* Show an error for out of range democratic cooldowns
* SubSonic - Force a default format (xml) instead of none
* Added back the agent string in recently played (for admins)
* Replace 'Admin' icon with padlock in sidebar when access check fails. (Hide this new icon with 'simple_user_mode')
* Disable API/Subsonic password resets in 'simple_user_mode'
* New option -m 'move_catalog' added to catalog_update.inc
* More default preferences to the refill/check functions
* More functions to search (album artist, mbid)

[1.2.0]
* Update Ampache to 4.4.0
* [Full changelog](https://github.com/ampache/ampache/releases/tag/4.4.0)
* Write metadata to mp3, flac and ogg files. Requires metaflac and vorbiscomment installed on Linux.
* Write images to mp3 and flac files. Also requires metaflac on linux.
* File tags can be updated from catalog management page.
* Configurable settings for "Gather Art".
* Configurable art search limit.
* User selectable artist and year filter for Spotify album searches
* User selectable limit for art searches.

[1.2.1]
* Fix for subsonic streaming

[1.2.2]
* Update Ampache to 4.4.2
* [Full changelog](https://github.com/ampache/ampache/releases/tag/4.4.2)
* If you have an MBID in your artist, use that for last.fm queries
* Larger artist images when you don't have a text summary available
* Expanded artist, album and podcast thumbnails to reduce blank space
* Update album tags first when you update artist tags

[1.2.3]
* Update Ampache to 4.4.3
* [Full changelog](https://github.com/ampache/ampache/releases/tag/4.4.3)
* Catalog::update_counts to manage catalog changes
* Gather more art files from your tags
* Allow RatingMatch plugin to rate Album->Artist (Originally Song->Album->Artist)
* CVE-2021-32644

[1.3.0]
* Update Ampache to 5.1.0
* [Full changelog](https://github.com/ampache/ampache/releases/tag/5.1.0)

[1.3.1]
* Update Ampache to 5.1.1
* [Full changelog](https://github.com/ampache/ampache/releases/tag/5.1.1)

[1.3.2]
* Update Ampache to 5.2.0
* [Full changelog](https://github.com/ampache/ampache/releases/tag/5.2.0)

[1.3.3]
* Fix apache configs to log correct IP

[1.3.4]
* Update Ampache to 5.2.1
* [Full changelog](https://github.com/ampache/ampache/releases/tag/5.2.1)

[1.4.0]
* Update Ampache to 5.3.0
* [Full changelog](https://github.com/ampache/ampache/releases/tag/5.3.0)

[1.4.1]
* Update Ampache to 5.3.1
* [Full changelog](https://github.com/ampache/ampache/releases/tag/5.3.1)

[1.4.2]
* Update Ampache to 5.3.2
* [Full changelog](https://github.com/ampache/ampache/releases/tag/5.3.2)

[1.4.3]
* Update Ampache to 5.3.3
* [Full changelog](https://github.com/ampache/ampache/releases/tag/5.3.3)
* Remove duplicates and order largest to smallest for art search
* Allow update_from_tags for a single Song from it's page
* Default art_search_limit raised to 15
* web_player shuffle improvements 

[1.5.0]
* Update Ampache to 5.4.0
* [Full changelog](https://github.com/ampache/ampache/releases/tag/5.4.0)

[1.5.1]
* Update Ampache to 5.4.1
* [Full changelog](https://github.com/ampache/ampache/releases/tag/5.4.1)
* Allow _ and % wildcards for hiding playlists (api_hidden_playlists)
* Missing translations on CLI strings
* web_player being unable to play different formats in some cases
* Playlist download button missing ID
* Truncate long podcast episode author lengths

[1.5.2]
* Update Ampache to 5.5.0
* [Full changelog](https://github.com/ampache/ampache/releases/tag/5.5.0)

[1.5.3]
* Update Ampache to 5.5.1
* [Full changelog](https://github.com/ampache/ampache/releases/tag/5.5.1)
* Translation Updates August 2022
* Grouping for label search items
* Release version string is incorrect and will tell you you have updates if you use the release files
* Missing comma between label links on song pages

[1.5.4]
* Update Ampache to 5.5.2
* [Full changelog](https://github.com/ampache/ampache/releases/tag/5.5.2)
* Do not overwrite a custom Artist/Album when updating from tags
* Ignore case when comparing Genre
* Show an error on share create failures
* Pull some function back into the search class
* When searching without rules treat it like a browse and return results unfiltered

[1.5.5]
* Update Ampache to 5.5.3
* [Full changelog](https://github.com/ampache/ampache/releases/tag/5.5.3)
* Update copyright year in footer.inc.php
* Localplay status and `instance_fields` function cleanup
* Update some docker files to match current images
* Allow adding streams to playlists (including rightbar)
* webplayer: Another code rework, remove the old 'original' list
* webplayer: Shuffle is an action instead of a state of the playlist

[1.5.6]
* Update Ampache to 5.5.4
* [Full changelog](https://github.com/ampache/ampache/releases/tag/5.5.4)
* Not filtering `song_artist` on `album_artist` browses
* Don't use `catalog_filter` and `rating_filter` without a valid user
* Uploaded/Manual Album Artist maps on tag update
* Delete artist's from the `catalog_map` that don't have a song or album for that catalog
* Set correct transcode bitrate and mime for songs on `play_url` calls

[1.5.7]
* Update Ampache to 5.5.5
* [Full changelog](https://github.com/ampache/ampache/releases/tag/5.5.5)
* Set etag for image cache
* Spotify art collector
* Double scrub string in catalog search rules

[1.5.8]
* Update Ampache to 5.5.6
* Update Cloudron base image to 4.0.0
* [Full changelog](https://github.com/ampache/ampache/releases/tag/5.5.6)
* Scrutinizer moved to php8.1
* Spotify art collector (AGAIN)
* get_now_playing has_access check
* Malformed HTML for regular users in preferences sidebar
* Missing translation on preferences sidebar
* Default catalog_filter group could be missing on a new install
* Gather genre tags when not an array
* Display webp images
* Check for a valid image extensions when uploading art
* Templates for squashed branch with a default path

[1.5.9]
* Update Ampache to 5.5.7
* [Full changelog](https://github.com/ampache/ampache/releases/tag/5.5.7)
* Stop filtering items beginning with a "." during catalog import
* Don't show the filter box if there aren't any filters for the page
* Fix up a lot of issues upgrading from really old servers
* Don't add Album maps for null Album Artist's
* Filter actions on the alphabet form and the graph pages correctly

[1.6.0]
* Update Ampache to 5.6.0
* [Full changelog](https://github.com/ampache/ampache/releases/tag/5.6.0)
* Subsonic Add type and serverVersion to <subsonic-response> objects
* Enforce raw format parameter on download links when not set
* Set song channels to null instead of 0 when missing
* Config had a : instead of a ; for a newline
* Webplayer missing semi colons, next / back keys in the playlist js
* Duckduckgo search links
* Register action missing catalog_filter_group
* LDAP DN lookup for group membership
* Identify object names correctly for localplay playlist items
* Parse URLs for democratic and random correctly in localplay playlist items
* Make sure the webplayer identies non-media stream types as valid
* Possibly unset Artist name in lyrics lookup
* Allow access to public smartlists in Random
* Share expiry date needed some reworking
* Search Use artist_map table for rating searches to find all related artists
* Subsonic Error's were not returning valid responses

[1.6.1]
* Update Ampache to 5.6.1
* [Full changelog](https://github.com/ampache/ampache/releases/tag/5.6.1)
* Simplified transcode settings checks
* Clean up the PlayAction class to make it a bit less complicated
* Encode URL's with a + for segmented play urls
* Soundcloud catalogs

[1.6.2]
* Update Ampache to 5.6.2
* [Full changelog](https://github.com/ampache/ampache/releases/tag/5.6.2)
* This update is the last `5.*` release before 6.0.0!
* Fork https://github.com/scaron/prettyphoto and update for jquery3
* Update webplayer to fix a longstanding Google Chrome issue with playing flac
* Being unable to view all your catalogs in the filter box
* Prettyphoto would rewrite your link when clicking on pictures

[1.7.0]
* Update Ampache to 6.0.1
* [Full changelog](https://github.com/ampache/ampache/releases/tag/6.0.0)

[1.7.1]
* Update Ampache to 6.0.3
* [Full changelog](https://github.com/ampache/ampache/releases/tag/6.0.3)
* Lyrist plugin regex be a bit looser with user input and regex /api/ on the end of api_host
* Don't try and load preferences page without a user to load
* Check for downsample_remote conditions on song play_url generation
* Don't downsample songs during a stream (play_url should catch this before you stream)
* Sort album browse pages based on your album_sort preference
* Error checking user temp playlist could give you a blank index page
* Runtime errors with missing data
* Missing translations for language list
* Select uploaded artists using the artist instead of song
* Missing column in Search::get_searches SQL
* Updating artist_map too much
* Last.fm lookup url was missing an & for albums
* Don't try to load an album_disk that doesn't have an album
* Restore sorting on album lists and browses that aren't grouped by release_type
* Catch Spotify runtime error on retry as well as initial attempt

[1.8.0]
* Update base image to 4.2.0

[1.9.0]
* Update Ampache to 6.1.0
* [Full changelog](https://github.com/ampache/ampache/releases/tag/6.1.0)

[1.9.1]
* Update Ampache to 6.2.0
* [Full changelog](https://github.com/ampache/ampache/releases/tag/6.2.0)

[1.9.2]
* Update Ampache to 6.2.1
* [Full changelog](https://github.com/ampache/ampache/releases/tag/6.2.1)
* Translations 2024-01
* Add podcast opml export to the podcasts page
* Advertise WebPlayer song info via MediaSession API / MPRIS

[1.10.0]
* Update Ampache to 6.3.0
* [Full changelog](https://github.com/ampache/ampache/releases/tag/6.3.0)

[1.10.1]
* Update Ampache to 6.3.1
* [Full changelog](https://github.com/ampache/ampache/releases/tag/6.3.1)
* Added an option to clean a folder path on the Show Catalogs page
* Show full playlist names on the personal favorite plugins (missing username)
* Block direct stream for shared file when share is disabled
* Config options write_id3 and write_id3_art don't do anything so remove them

[1.11.0]
* Update Ampache to 6.4.0
* [Full changelog](https://github.com/ampache/ampache/releases/tag/6.4.0)
* **Check your config file encode_args**. Double check your config and remove the K from `%BITRATE%K`.
* Changes to random searches in the WebUI
* Translations 2024-05
* rtrim slashes on some config variables in case you ignore the warning
* Stream Random action default fallback to song
* Allow using `tmp_dir_path` for Dropbox catalog

[1.12.0]
* Fix config to have the latest version
* Disable update checker

[1.13.0]
* Update Ampache to 6.5.0
* [Full changelog](https://github.com/ampache/ampache/releases/tag/6.5.0)

[1.14.0]
* Update Ampache to 6.6.0
* [Full changelog](https://github.com/ampache/ampache/releases/tag/6.6.0)

[1.14.1]
* Update Ampache to 6.6.1
* [Full changelog](https://github.com/ampache/ampache/releases/tag/6.6.1)

[1.14.2]
* Update Ampache to 6.6.3
* [Full changelog](https://github.com/ampache/ampache/releases/tag/6.6.3)

[1.14.3]
* Update Ampache to 6.6.4
* [Full changelog](https://github.com/ampache/ampache/releases/tag/6.6.4)

[1.15.0]
* Update Ampache to 7.0.0
* [Full changelog](https://github.com/ampache/ampache/releases/tag/7.0.0)

[1.15.1]
* Update Ampache to 7.0.1
* [Full changelog](https://github.com/ampache/ampache/releases/tag/7.0.1)

[1.16.0]
* Update ampache to 7.1.0
* [Full Changelog](https://github.com/ampache/ampache/releases/tag/7.1.0)
* Translations 2024-12-11
* Build PHP8.4 supported release zips
* Rating and favorite flags are now using SVG's
* Update Composer and NPM packages
* Discogs search icon to valid library items
* Add refresh icons to each dashboard page row
* Add refresh icons to each home dashboard plugin page row
* Update Creative Commons 3.0 licenses and include 4.0 versions
* Debug messages from cron process
* Don't show delete icon for user tokens when they don't have one
* Allow editing AlbumDisk objects directly
* Add AlbumDisk edit links to each disk on group pages
* Add AlbumDisk `disk` `disksubtitle` properties to single disk Album edit windows
* Allow editing song `disk`
* Define missing abstract method for beets catalog
* Allow a wild (`.*`) regex filter on Alphabet filters

[1.16.1]
* Update ampache to 7.1.1
* [Full Changelog](https://github.com/ampache/ampache/releases/tag/7.1.1)
* Upload POST array not sending license correctly to filepond

[1.17.0]
* Update ampache to 7.2.0
* [Full Changelog](https://github.com/ampache/ampache/releases/tag/7.2.0)
* Add `npm audit` to GitHub QA workflow
* Assign a HTML id to input text on inline loaded input
* Add `[No Genre]` as the first genre on the browse pages

[1.18.0]
* Update ampache to 7.3.0
* [Full Changelog](https://github.com/ampache/ampache/releases/tag/7.3.0)
* Translations 2025-02-14
* Added option to show separate Artist column for playlist media
* Pre-translate common strings on repeated tasks before loading media row templates
* Clean empty albums after each verify chunk
* Add `memory_get_peak_usage` to query stat output
* Add `.htaccess.dist` to the web root. (Block obviously bad parameters)
* HTML link on Share list
* New catalog Auto-insert Fields
* Search
* Database 721001
* Update NPM `vite` package
* Update table counts for `album` and `artist` objects on update
* Clean up empty albums during migration
* Rework catalog_map insert and remove during file updates

