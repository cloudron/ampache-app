#!/bin/bash

set -eu

readonly mysql="mysql --user=${CLOUDRON_MYSQL_USERNAME} --password=${CLOUDRON_MYSQL_PASSWORD} --host=${CLOUDRON_MYSQL_HOST} ${CLOUDRON_MYSQL_DATABASE}"

# for logs to work, one must set debug=true in the config
mkdir -p /run/ampache/sessions /app/data/rest /app/data/play /app/data/themes /run/ampache/logs

function update_config() {
    if [[ ! -f /app/data/config/ampache.cfg.php ]]; then
        echo "==> Generating initial config"
        sed -e "s/^secret_key = .*/secret_key = \"$(pwgen -1s 32)\"/" /app/pkg/ampache.cfg.php.template > /app/data/config/ampache.cfg.php
    fi

    # Path Vars
    sed -e "s/^http_host = .*$/http_host =  \"${CLOUDRON_APP_DOMAIN}\"/" \
        -e "s,^local_web_path = .*$,local_web_path = http://localhost:8000," \
        -i /app/data/config/ampache.cfg.php

    # config version
    upstream_version=$(sed -ne 's/^config_version = \(.*\)/\1/p' /app/code/config.orig/ampache.cfg.php.dist)
    sed -e "s/^config_version = .*$/config_version = ${upstream_version}/" -i /app/data/config/ampache.cfg.php

    # database
    sed -e "s/^database_hostname = .*/database_hostname = \"${CLOUDRON_MYSQL_HOST}\"/" \
        -e "s/^database_port = .*/database_port = \"${CLOUDRON_MYSQL_PORT}\"/" \
        -e "s/^database_name = .*/database_name = \"${CLOUDRON_MYSQL_DATABASE}\"/" \
        -e "s/^database_username =.*/database_username = \"${CLOUDRON_MYSQL_USERNAME}\"/" \
        -e "s/^database_password = .*/database_password = \"${CLOUDRON_MYSQL_PASSWORD}\"/" \
        -i /app/data/config/ampache.cfg.php

    # LDAP and Public Registration settings
    # auth_password_save will update the password field in the db. this is required for API/subsonic access
    sed -e "s,^ldap_url = .*,ldap_url = \"${CLOUDRON_LDAP_URL}\"," \
        -e "s/^ldap_username = .*/ldap_username = \"${CLOUDRON_LDAP_BIND_DN}\"/" \
        -e "s/^ldap_password = .*/ldap_password = \"${CLOUDRON_LDAP_BIND_PASSWORD}\"/" \
        -e "s/^ldap_search_dn = .*/ldap_search_dn = \"${CLOUDRON_LDAP_USERS_BASE_DN}\"/" \
        -e "s/^ldap_objectclass = .*/ldap_objectclass = \"user\"/" \
        -e "s/^ldap_filter = .*/ldap_filter = \"(username=%v)\"/" \
        -e "s/^ldap_name_field = .*/ldap_name_field = \"displayname\"/" \
        -e "s/^ldap_email_field = .*/ldap_email_field = \"mail\"/" \
        -i /app/data/config/ampache.cfg.php

    # mail/SMTP . https://tldp.org/LDP/abs/html/string-manipulation.html for the %% magic
    sed -e "s/^mail_enable = .*/mail_enable = \"true\"/" \
        -e "s/^mail_type = .*/mail_type = \"smtp\"/" \
        -e "s/^mail_domain = .*/mail_domain = \"${CLOUDRON_MAIL_DOMAIN}\"/" \
        -e "s/^mail_user = .*/mail_user = \"${CLOUDRON_MAIL_FROM%%@*}\"/" \
        -e "s/^mail_name = .*/mail_name = \"${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-Ampache}\"/" \
        -e "s/^mail_host = .*/mail_host = \"${CLOUDRON_MAIL_SMTP_SERVER}\"/" \
        -e "s/^mail_port = .*/mail_port = \"${CLOUDRON_MAIL_SMTP_PORT}\"/" \
        -e "s/^mail_secure_smtp = .*/mail_secure_smtp = \"none\"/" \
        -e "s/^mail_auth = .*/mail_auth = \"true\"/" \
        -e "s/^mail_auth_user = .*/mail_auth_user = \"${CLOUDRON_MAIL_SMTP_USERNAME}\"/" \
        -e "s/^mail_auth_pass = .*/mail_auth_pass = \"${CLOUDRON_MAIL_SMTP_PASSWORD}\"/" \
        -i /app/data/config/ampache.cfg.php
}

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

# symlink the themes
echo "==> Symlinking themes"
for f in $(ls /app/code/public/themes.orig); do
    rm -rf "/app/data/themes/$f"
    ln -sf "/app/code/public/themes.orig/$f" "/app/data/themes/$f"
done

if [[ ! -d /app/data/config ]]; then
    echo "==> Detected first run"

    # create database
    echo "==> Initializing database"
    $mysql < /app/code/resources/sql/ampache.sql

    # copy config
    cp -r /app/code/config.orig /app/data/config
    cp /app/code/public/rest/.htaccess.dist /app/data/rest/htaccess
    cp /app/code/public/play/.htaccess.dist /app/data/play/htaccess

    update_config

    echo "==> Updating database"
    /app/code/bin/cli admin:updateDatabase --execute

    # -l is access level
    echo "==> Creating admin user"
    /app/code/bin/cli admin:addUser admin --level 100 --password changeme --email admin@cloudron.local --name "Ampache Admin"

    # 95 is 'autoupdate' hardcoded in src/Repository/Model/Preference.php
    $mysql -e "UPDATE user_preference SET value=0 WHERE preference=95"
    $mysql -e "UPDATE preference SET value=0 WHERE id=95"
else
    echo "==> Updating existing installation"
    update_config

    echo "==> Updating database"
    /app/code/bin/cli admin:updateDatabase --execute
fi

echo "==> Changing ownership"
chown -R www-data.www-data /app/data /run/ampache 

echo "==> Starting ampache"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"

exec /usr/sbin/apache2 -DFOREGROUND
