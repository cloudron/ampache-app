## About

Ampache is a web based audio/video streaming application and file manager allowing you to access your
music & videos from anywhere, using almost any internet enabled device.

Ampache's usefulness is heavily dependent on being able to extract correct metadata from embedded tags in your
files and/or the file name. Ampache is not a media organiser; it is meant to be a tool which presents an already
organised collection in a useful way. It assumes that you know best how to manage your files and are capable of
choosing a suitable method for doing so.

## Features

* Music collection - Browse and manage your music collection through a simple web interface. Synchronize local and remote catalogs to an unique consistent collection.
* Music streaming - Stream your music to your preferred player, control it with Localplay or directly listen on the web page with HTML5 player.
* Open source - Completely Free and Open Source since 2001, AGPLv3 license. Get involved and take control.
* Ampache everywhere - Listen to your music from your phone, tablet or television. At home, at work or in vacation: get Ampache everywhere using a compatible client!

